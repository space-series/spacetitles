package dev.spaceseries.spacetitles.text;

import com.google.common.base.Joiner;
import dev.spaceseries.spacetitles.SpaceTitles;
import dev.spaceseries.spacetitles.util.TitlesConfig;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;

public class TitleExpansion extends PlaceholderExpansion {
    public TitleExpansion() {
    }

    @Override
    public String onPlaceholderRequest(Player player, String params) {

        TitleManager.TitleStatus status = TitleManager.getInstance().getStatus(player.getUniqueId());
        TitleManager.Title title = status == null ? null : status.getTitle();

        switch (params) {
            case "prefix":
                if (title != null && title.getPrefix() != null) {
                    return title.getPrefix();
                }
                break;
            case "suffix":
                if (title != null && title.getSuffix() != null) {
                    return title.getSuffix();
                }
                break;
            case "color":
                if (status != null && status.getColor() != null) {
                    return status.getColor();
                } else if (title != null && title.getColor() != null) {
                    return title.getColor();
                }
                break;
            case "chatcolor":
                if (status != null && status.getChatColor() != null) {
                    return status.getChatColor();
                } else {
                    return TitlesConfig.DEFAULT_CHAT_COLOR.get(SpaceTitles.getInstance().getConfiguration().getConfig());
                }
            case "default_color":
                return TitlesConfig.DEFAULT_NAME_COLOR.get(SpaceTitles.getInstance().getConfiguration().getConfig());
        }

        return "";
    }

    @Override
    public String getIdentifier() {
        return "title";
    }

    @Override
    public String getAuthor() {
        return Joiner.on(", ").join(SpaceTitles.getInstance().getDescription().getAuthors());
    }

    @Override
    public String getVersion() {
        return SpaceTitles.getInstance().getDescription().getVersion();
    }
}
