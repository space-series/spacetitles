package dev.spaceseries.spacetitles.text;

import dev.spaceseries.spacetitles.SpaceTitles;
import dev.spaceseries.spacetitles.listener.ChatListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TitleManager {

    private static TitleManager instance;

    public static TitleManager getInstance() {
        if (instance == null) instance = new TitleManager();
        return instance;
    }

    private TitleManager() {
        new BukkitRunnable() {
            @Override
            public void run() {
                load();
            }
        }.runTaskTimerAsynchronously(SpaceTitles.getInstance(), 1L, 300L);
        Bukkit.getServer().getPluginManager().registerEvents(new ChatListener(), SpaceTitles.getInstance());
    }

    private Map<String, Title> titles = new ConcurrentHashMap<>();
    private Map<UUID, TitleStatus> titleStatusMap = new ConcurrentHashMap<>();

    private void load() {
        try (Connection connection = SpaceTitles.getInstance().getStorage().getConnection()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet rs = statement.executeQuery("SELECT * FROM space_titles");

                while (rs.next()) {
                    String name = rs.getString("name");
                    String prefix = rs.getString("prefix");
                    String color = rs.getString("color");
                    String suffix = rs.getString("suffix");
                    boolean visible = rs.getBoolean("visible");

                    titles.put(name.toLowerCase(), new Title(name, prefix, color, suffix, visible));
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void loadStatus(UUID uuid) {
        try (Connection connection = SpaceTitles.getInstance().getStorage().getConnection()) {
            String sql = "SELECT * FROM space_player_titles WHERE uuid = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, uuid.toString());

                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    String title = rs.getString("name");
                    String color = rs.getString("color");
                    String chatColor = rs.getString("chatcolor");

                    titleStatusMap.put(uuid, new TitleStatus(title, color, chatColor));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public TitleStatus getStatus(UUID uuid) {
        return titleStatusMap.getOrDefault(uuid, null);
    }

    public Collection<Title> getTitles() {
        return titles.values();
    }

    public Title getTitle(String arg) {
        return titles.getOrDefault(arg.toLowerCase(), null);
    }

    public void removeTitle(Player sender) {
        new BukkitRunnable() {
            @Override
            public void run() {
                UUID uuid = sender.getUniqueId();
                try (Connection connection = SpaceTitles.getInstance().getStorage().getConnection()) {
                    String sql = "INSERT INTO space_player_titles (uuid, name, color) VALUES (?, ?, ?) " +
                            "ON DUPLICATE KEY UPDATE name = VALUES(name)";
                    try (PreparedStatement statement = connection.prepareStatement(sql)) {
                        statement.setString(1, uuid.toString());
                        statement.setNull(2, Types.VARCHAR);
                        statement.setNull(3, Types.VARCHAR);

                        statement.executeUpdate();
                    }
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }

                loadStatus(uuid);
            }
        }.runTaskAsynchronously(SpaceTitles.getInstance());
    }

    public void reload() {
        load();
    }

    public void setColor(UUID uuid, ChatColor color) {
        new UpdateTask(uuid, null, "&" + color.getChar(), null,false, true, false);
    }

    public void removeColor(Player sender) {
        new UpdateTask(sender.getUniqueId(), null, null, null, false, true, false);
    }

    public void setChatColor(UUID uuid, ChatColor chatColor) {
        new UpdateTask(uuid, null, null, "&" + chatColor.getChar(),false, false, true);
    }

    public void removeChatColor(Player sender) {
        new UpdateTask(sender.getUniqueId(), null, null, null, false, false, true);
    }

    public class Title implements Comparable<Title> {
        private final String handle;
        private final boolean visible;

        private String prefix;
        private String suffix;
        private String color;

        Title(String handle, String prefix, String color, String suffix, boolean visible) {
            this.handle = handle;
            if (prefix != null)
                this.prefix = ChatColor.translateAlternateColorCodes('&', prefix);
            if (suffix != null)
                this.suffix = ChatColor.translateAlternateColorCodes('&', suffix);
            if (color != null)
                this.color = ChatColor.translateAlternateColorCodes('&', color);

            this.visible = visible;
        }

        public String getHandle() {
            return handle;
        }

        public String getPrefix() {
            return prefix;
        }

        public String getSuffix() {
            return suffix;
        }

        public String getColor() {
            return color;
        }

        public boolean isVisible() {
            return visible;
        }

        public String preview(String name) {
            StringBuilder sb = new StringBuilder();

            if (prefix != null)
                sb.append(prefix);

            if (color != null)
                sb.append(color);

            sb.append(name);

            if (suffix != null)
                sb.append(suffix);

            return sb.toString();
        }

        public void set(Player player) {
            setTitle(player.getUniqueId(), getHandle());
        }

        @Override
        public int compareTo(Title o) {
            if (o == null) throw new NullPointerException();
            return this.handle.compareTo(o.handle);
        }
    }

    private void setTitle(UUID uuid, String handle) {
        new UpdateTask(uuid, handle, null, null, true, false, false);
    }

    public class TitleStatus {
        private String title;
        private String color;
        private String chatColor;

        public TitleStatus(String title, String color, String chatColor) {
            this.title = title;
            this.color = color;
            this.chatColor = chatColor;
        }

        public Title getTitle() {
            if (title == null) return null;
            return titles.getOrDefault(title, null);
        }

        public String getColor() {
            return color == null ? null : ChatColor.translateAlternateColorCodes('&', color);
        }

        public String getChatColor() {
            return chatColor == null ? null : ChatColor.translateAlternateColorCodes('&', chatColor);
        }
    }

    public class UpdateTask extends BukkitRunnable {

        private final UUID uuid;
        private final String title;
        private final String color;
        private final String chatColor;
        private final boolean updateTitle;
        private final boolean updateColor;
        private final boolean updateChatColor;

        public UpdateTask(UUID uuid, String title, String color, String chatColor,
                          boolean updateTitle, boolean updateColor, boolean updateChatColor) {
            this.uuid = uuid;
            this.title = title;
            this.color = color;
            this.chatColor = chatColor;
            this.updateTitle = updateTitle;
            this.updateColor = updateColor;
            this.updateChatColor = updateChatColor;
            runTaskAsynchronously(SpaceTitles.getInstance());
        }

        @Override
        public void run() {
            String sql;
            if (updateTitle) {
                sql = "INSERT INTO space_player_titles (uuid, name, color, chatcolor) VALUES (?, ?, ?, ?) " +
                        "ON DUPLICATE KEY UPDATE name = VALUES(name)";
            } else if (updateColor) {
                sql = "INSERT INTO space_player_titles (uuid, name, color, chatcolor) VALUES (?, ?, ?, ?) " +
                        "ON DUPLICATE KEY UPDATE color = VALUES(color)";
            } else if (updateChatColor) {
                sql = "INSERT INTO space_player_titles (uuid, name, color, chatcolor) VALUES (?, ?, ?, ?) " +
                        "ON DUPLICATE KEY UPDATE chatcolor = VALUES(chatcolor)";
            } else return;

            try (Connection connection = SpaceTitles.getInstance().getStorage().getConnection()) {
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.setString(1, uuid.toString());
                    if (title == null) {
                        statement.setNull(2, Types.VARCHAR);
                    } else {
                        statement.setString(2, title);
                    }
                    if (color == null) {
                        statement.setNull(3, Types.VARCHAR);
                    } else {
                        statement.setString(3, color);
                    }
                    if (chatColor == null) {
                        statement.setNull(4, Types.VARCHAR);
                    } else {
                        statement.setString(4, chatColor);
                    }

                    statement.executeUpdate();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            loadStatus(uuid);
        }
    }
}
