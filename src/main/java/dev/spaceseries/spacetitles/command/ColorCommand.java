package dev.spaceseries.spacetitles.command;

import dev.spaceseries.api.abstraction.plugin.Plugin;
import dev.spaceseries.api.command.*;
import dev.spaceseries.api.gui.Gui;
import dev.spaceseries.api.util.ItemBuilder;
import dev.spaceseries.spacetitles.SpaceTitles;
import dev.spaceseries.spacetitles.text.TitleManager;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import static dev.spaceseries.spacetitles.util.Messages.*;

@PlayersOnly
public class ColorCommand extends Command {

    private final Plugin plugin;
    private Gui gui;

    public ColorCommand(Plugin plugin) {
        super(plugin, "color");
        this.plugin = plugin;

        addSubCommands(new Set(), new Remove());

        loadGui();
    }

    private void loadGui() {
        int lines = 3;

        this.gui = new Gui(SpaceTitles.getInstance(), TextComponent.toLegacyText(COLOR_GUI_TITLE.toBaseComponents().get(0)), lines);

        gui.addItemInteraction(p -> new ItemBuilder(Material.WOOL)
                .setDurability((short) 14)
                .setName(ChatColor.RED + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set red");
        });

        gui.addItemInteraction(p -> new ItemBuilder(Material.STAINED_CLAY)
                .setDurability((short) 14)
                .setName(ChatColor.DARK_RED + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set red");
        });

        gui.addItemInteraction(p -> new ItemBuilder(Material.WOOL)
                .setDurability((short) 4)
                .setName(ChatColor.YELLOW + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set yellow");
        });

        gui.addItemInteraction(p -> new ItemBuilder(Material.WOOL)
                .setDurability((short) 5)
                .setName(ChatColor.GREEN + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set green");
        });

        gui.addItemInteraction(p -> new ItemBuilder(Material.WOOL)
                .setDurability((short) 13)
                .setName(ChatColor.DARK_GREEN + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set dark_green");
        });

        gui.addItemInteraction(p -> new ItemBuilder(Material.WOOL)
                .setDurability((short) 2)
                .setName(ChatColor.LIGHT_PURPLE + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set light_purple");
        });

        gui.addItemInteraction(p -> new ItemBuilder(Material.WOOL)
                .setDurability((short) 1)
                .setName(ChatColor.GOLD + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set gold");
        });

        gui.addItemInteraction(p -> new ItemBuilder(Material.WOOL)
                .setDurability((short) 3)
                .setName(ChatColor.AQUA + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set aqua");
        });

        gui.addItemInteraction(p -> new ItemBuilder(Material.WOOL)
                .setDurability((short) 9)
                .setName(ChatColor.DARK_AQUA + p.getName())
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color set dark_aqua");
        });

        gui.setItemInteraction(22, p -> new ItemBuilder(Material.BARRIER)
                .setName(TextComponent.toLegacyText(REMOVE_BUTTON.toBaseComponents().get(0)), true)
                .toItemStack(), (p, e) -> {
            p.closeInventory();
            Bukkit.dispatchCommand(p, "color remove");
        });

    }

    @Override
    public void onCommand(SpaceCommandSender sender, String s, String... args) {
        gui.open(((Player) ((BukkitSpaceCommandSender) sender).getBukkitSender()));
    }

    @SubCommand
    private class Set extends Command {
        Set() {
            super(plugin, "set");
        }

        @Override
        public void onCommand(SpaceCommandSender sender, String s, String... args) {
            if (args.length < 1) {
                Bukkit.dispatchCommand(((BukkitSpaceCommandSender) sender).getBukkitSender(), "color");
                return;
            }

            ChatColor color;
            try {
                color = ChatColor.valueOf(args[0].toUpperCase());
            } catch (Exception e) {
                NO_SUCH_COLOR.msg(sender);
                return;
            }

            if (!sender.hasPermission("space.color.*") && !sender.hasPermission("space.color." + color.name())) {
                COLOR_NOT_PURCHASED.msg(sender);
                return;
            }

            TitleManager.getInstance().setColor(sender.getUuid(), color);
            COLOR_SET.msg(sender, "%color%", color + sender.getName());

        }
    }

    @SubCommand
    private class Remove extends Command {
        Remove() {
            super(plugin, "remove");
        }

        @Override
        public void onCommand(SpaceCommandSender sender, String s, String... args) {
            TitleManager.getInstance().removeColor(((Player) ((BukkitSpaceCommandSender) sender).getBukkitSender()));
            COLOR_REMOVED.msg(sender);
        }
    }
}
