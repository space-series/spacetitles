package dev.spaceseries.spacetitles.command;

import dev.spaceseries.api.abstraction.plugin.Plugin;
import dev.spaceseries.api.command.*;
import dev.spaceseries.api.gui.Gui;
import dev.spaceseries.api.util.ItemBuilder;
import dev.spaceseries.spacetitles.SpaceTitles;
import dev.spaceseries.spacetitles.text.TitleManager;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.stream.Collectors;

import static dev.spaceseries.spacetitles.util.Messages.*;

@PlayersOnly
public class TitleCommand extends Command {

    public TitleCommand(Plugin plugin) {
        super(plugin, "title");

        addSubCommands(new Set(), new Remove(), new Reload());
    }

    @Override
    public void onCommand(SpaceCommandSender sender, String label, String[] args) {
        Collection<TitleManager.Title> titles = TitleManager.getInstance().getTitles().stream()
                .filter(TitleManager.Title::isVisible)
                .collect(Collectors.toSet());
        int size = titles.size() + 1;

        int lines = size == 0 ? 1 : size / 9 + (size % 9 == 0 ? 0 : 1);

        Gui gui = new Gui(SpaceTitles.getInstance(), TextComponent.toLegacyText(TITLE_GUI_TITLE.toBaseComponents().get(0)), lines);

        for (TitleManager.Title title : titles) {
            gui.addItemInteraction(p ->
                            p.hasPermission("space.title." + title.getHandle()) || p.hasPermission("space.title.*") ?
                                    new ItemBuilder(Material.NAME_TAG)
                                            .setName(title.preview(p.getName()))
                                            .toItemStack() :
                                    new ItemBuilder(Material.BEDROCK)
                                            .setName(title.preview(p.getName()))
                                            .toItemStack(),
                    (p, e) -> {
                        p.closeInventory();
                        Bukkit.dispatchCommand(p, "title set " + title.getHandle());
                    });
        }

        gui.setItemInteraction(lines * 9 - 1, new ItemBuilder(Material.BARRIER)
                        .setName(TextComponent.toLegacyText(REMOVE_BUTTON.toBaseComponents().get(0))).toItemStack(),
                (p, e) -> {
                    p.closeInventory();
                    Bukkit.dispatchCommand(p, "title remove");
                });

        gui.open(((Player) ((BukkitSpaceCommandSender) sender).getBukkitSender()));
    }

    @SubCommand
    private class Set extends Command {

        public Set() {
            super(TitleCommand.this.getPlugin(),"set");
        }

        @Override
        public void onCommand(SpaceCommandSender sender, String label, String[] args) {
            if (args.length < 1) {
                Bukkit.dispatchCommand(((BukkitSpaceCommandSender) sender).getBukkitSender(), "title");
                return;
            }

            TitleManager.Title title = TitleManager.getInstance().getTitle(args[0]);
            if (title == null) {
                NO_SUCH_TITLE.msg(sender);
                return;
            }

            if (!sender.hasPermission("space.title.*") &&
                    !sender.hasPermission("space.title." + title.getHandle())) {
                TITLE_NOT_PURCHASED.msg(sender);
                return;
            }

            title.set(((Player) ((BukkitSpaceCommandSender) sender).getBukkitSender()));
            TITLE_SET.msg(sender, "%title%", title.preview(sender.getName()));
        }
    }

    @SubCommand
    @PlayersOnly
    private class Remove extends Command {
        public Remove() {
            super(TitleCommand.this.getPlugin(), "remove");
        }

        @Override
        public void onCommand(SpaceCommandSender sender, String label, String[] args) {
            TitleManager.getInstance().removeTitle(((Player) ((BukkitSpaceCommandSender) sender).getBukkitSender()));
            TITLE_REMOVED.msg(sender);
        }
    }

    @SubCommand
    @Permissible("space.title.reload")
    private class Reload extends Command {

        public Reload() {
            super(TitleCommand.this.getPlugin(), "reload");
        }

        @Override
        public void onCommand(SpaceCommandSender sender, String label, String[] args) {
            TitleManager.getInstance().reload();
        }
    }
}
