package dev.spaceseries.spacetitles;

import dev.spaceseries.api.abstraction.plugin.BukkitPlugin;
import dev.spaceseries.api.gui.listener.GuiListener;
import dev.spaceseries.spacetitles.command.ChatColorCommand;
import dev.spaceseries.spacetitles.command.ColorCommand;
import dev.spaceseries.spacetitles.command.TitleCommand;
import dev.spaceseries.spacetitles.storage.MySQLStorage;
import dev.spaceseries.spacetitles.text.TitleExpansion;
import dev.spaceseries.spacetitles.text.TitleManager;
import dev.spaceseries.spacetitles.util.TitlesConfig;
import dev.spaceseries.spacetitles.util.LangConfig;
import org.bukkit.plugin.java.JavaPlugin;

import static dev.spaceseries.spacetitles.util.TitlesConfig.*;

public class SpaceTitles extends JavaPlugin {

    private static SpaceTitles instance;

    private TitlesConfig config;

    private LangConfig langConfig;

    private MySQLStorage storage;

    public static SpaceTitles getInstance() {
        return instance;
    }

    public SpaceTitles() {
        instance = this;
    }

    @Override
    public void onEnable() {
        super.onEnable();

        BukkitPlugin plugin = new BukkitPlugin(this);

        config = new TitlesConfig(plugin);
        langConfig = new LangConfig(plugin);

        this.storage = new MySQLStorage(
                DB_HOSTNAME.get(config.getConfig()), DB_PORT.get(config.getConfig()),
                DB_USERNAME.get(config.getConfig()), DB_PASSWORD.get(config.getConfig()),
                DB_NAME.get(config.getConfig())
        );

        new ColorCommand(plugin);
        new ChatColorCommand(plugin);
        new TitleCommand(plugin);

        TitleManager.getInstance();

        new TitleExpansion().register();

        new GuiListener(this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public TitlesConfig getConfiguration() {
        return config;
    }

    public MySQLStorage getStorage() {
        return storage;
    }

    public LangConfig getLangConfig() {
        return langConfig;
    }
}
