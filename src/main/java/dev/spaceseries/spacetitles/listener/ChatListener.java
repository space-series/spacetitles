package dev.spaceseries.spacetitles.listener;

import dev.spaceseries.spacetitles.SpaceTitles;
import dev.spaceseries.spacetitles.util.TitlesConfig;
import me.clip.placeholderapi.PlaceholderAPI;
import dev.spaceseries.spacetitles.text.TitleManager;
import dev.spaceseries.spacetitles.util.PermissionUtil;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class ChatListener implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        TitleManager.getInstance().loadStatus(event.getUniqueId());
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        // get tabList format
        String tablistFormat = TitlesConfig.TABLIST_FORMAT.get(SpaceTitles.getInstance().getConfiguration().getConfig());
        // replace placeholders
        tablistFormat = PlaceholderAPI.setPlaceholders(player, tablistFormat);

        player.setPlayerListName(tablistFormat);
    }

    private String evaluateDisplayName(Player player) {
        TitleManager.TitleStatus status = TitleManager.getInstance().getStatus(player.getUniqueId());

        StringBuilder displayName = new StringBuilder();
        displayName.append(player.getName());

        String prefix = PermissionUtil.getPrefix(player);

        boolean noPrefix = prefix.equalsIgnoreCase("");

        if (status != null) {
            if (status.getColor() != null) {
                displayName.insert(0, status.getColor());
                noPrefix = false;
            }
        }

        if (noPrefix)
            displayName.insert(0, ChatColor.GRAY);

        displayName.insert(0, prefix);

        return displayName.toString();
    }
}