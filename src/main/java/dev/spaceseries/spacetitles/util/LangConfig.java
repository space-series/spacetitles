package dev.spaceseries.spacetitles.util;

import dev.spaceseries.api.abstraction.plugin.Plugin;
import dev.spaceseries.api.config.obj.Config;

public class LangConfig extends Config {
    public LangConfig(Plugin plugin) {
        super(plugin, "lang.yml");
    }
}
