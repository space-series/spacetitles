package dev.spaceseries.spacetitles.util;

import dev.spaceseries.api.text.Message;
import dev.spaceseries.spacetitles.SpaceTitles;

public class Messages {
    public static final Message REMOVE_BUTTON = fromLangConfig("global.remove-button");

    public static final Message TITLE_GUI_TITLE = fromLangConfig("title.gui-title");
    public static final Message NO_SUCH_TITLE = fromLangConfig("title.no-such-title");
    public static final Message TITLE_NOT_PURCHASED = fromLangConfig("title.not-purchased");
    public static final Message TITLE_SET = fromLangConfig("title.title-set");
    public static final Message TITLE_REMOVED = fromLangConfig("title.title-removed");

    public static final Message COLOR_GUI_TITLE = fromLangConfig("color.gui-title");
    public static final Message NO_SUCH_COLOR = fromLangConfig("color.no-such-color");
    public static final Message COLOR_NOT_PURCHASED = fromLangConfig("color.not-purchased");
    public static final Message COLOR_SET = fromLangConfig("color.color-set");
    public static final Message COLOR_REMOVED = fromLangConfig("color.color-removed");

    public static final Message CHATCOLOR_GUI_TITLE = fromLangConfig("chatcolor.gui-title");
    public static final Message NO_SUCH_CHATCOLOR = fromLangConfig("chatcolor.no-such-color");
    public static final Message CHATCOLOR_NOT_PURCHASED = fromLangConfig("chatcolor.not-purchased");
    public static final Message CHATCOLOR_SET = fromLangConfig("chatcolor.chatcolor-set");
    public static final Message CHATCOLOR_REMOVED = fromLangConfig("chatcolor.chatcolor-removed");

    private static Message fromLangConfig(String ident) {
        return Message.fromConfigurationSection(
                SpaceTitles.getInstance().getLangConfig().getConfig().getSection(ident), ident
        ).build();
    }
}
