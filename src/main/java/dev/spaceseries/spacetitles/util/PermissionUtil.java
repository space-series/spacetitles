package dev.spaceseries.spacetitles.util;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import net.luckperms.api.query.QueryMode;
import net.luckperms.api.query.QueryOptions;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class PermissionUtil {
    private static LuckPerms getApi() {
        RegisteredServiceProvider<LuckPerms> registration = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
        if (registration == null) throw new RuntimeException("Luckperms not loaded!");

        return registration.getProvider();
    }

    private static User getUser(UUID uuid) {
        LuckPerms api = getApi();

        User user = api.getUserManager().getUser(uuid);
        if (user == null) {
            try {
                api.getUserManager().loadUser(uuid).get();
                user = api.getUserManager().getUser(uuid);
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        }

        return user;

    }

    private static Group getGroup(String group) {
        return getApi().getGroupManager().getGroup(group);
    }

    public static int getWeight(Player player) {
        User user = getUser(player.getUniqueId());
        if (user == null) return 0;

        Group group = getGroup(user.getPrimaryGroup());
        if (group == null) return 0;

        return group.getWeight().orElse(0);
    }

    public static String getPrefix(Player player) {
        User user = getUser(player.getUniqueId());
        if (user == null) return "";

        String prefix = user.getCachedData().getMetaData(QueryOptions.builder(QueryMode.NON_CONTEXTUAL).build()).getPrefix();
        return prefix == null ? "" : ChatColor.translateAlternateColorCodes('&', prefix);
    }
}
