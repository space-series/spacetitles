package dev.spaceseries.spacetitles.util;

import dev.spaceseries.api.abstraction.plugin.Plugin;
import dev.spaceseries.api.config.keys.ConfigKey;
import dev.spaceseries.api.config.keys.ConfigKeyTypes;
import dev.spaceseries.api.config.obj.Config;

public class TitlesConfig extends Config {

    public static final ConfigKey<String> DB_HOSTNAME = ConfigKeyTypes.stringKey("database.hostname", "localhost");
    public static final ConfigKey<Integer> DB_PORT = ConfigKeyTypes.integerKey("database.port", 3306);
    public static final ConfigKey<String> DB_USERNAME = ConfigKeyTypes.stringKey("database.username", "mysqlUsername");
    public static final ConfigKey<String> DB_PASSWORD = ConfigKeyTypes.stringKey("database.password", "mysqlPassword");
    public static final ConfigKey<String> DB_NAME = ConfigKeyTypes.stringKey("database.name", "mysqlDatabase");

    public static final ConfigKey<String> TABLIST_FORMAT = ConfigKeyTypes.stringKey("tablist-format", null);

    public static final ConfigKey<String> DEFAULT_CHAT_COLOR = ConfigKeyTypes.stringKey("default-chat-color", null);
    public static final ConfigKey<String> DEFAULT_NAME_COLOR = ConfigKeyTypes.stringKey("default-name-color", null);

    public TitlesConfig(Plugin plugin) {
        super(plugin, "config.yml");
    }
}
